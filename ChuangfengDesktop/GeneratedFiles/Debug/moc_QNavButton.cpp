/****************************************************************************
** Meta object code from reading C++ file 'QNavButton.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../CustomCtrl/QNavButton.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QNavButton.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_NavButton_t {
    QByteArrayData data[95];
    char stringdata0[1389];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_NavButton_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_NavButton_t qt_meta_stringdata_NavButton = {
    {
QT_MOC_LITERAL(0, 0, 9), // "NavButton"
QT_MOC_LITERAL(1, 10, 14), // "setPaddingLeft"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 11), // "paddingLeft"
QT_MOC_LITERAL(4, 38, 15), // "setPaddingRight"
QT_MOC_LITERAL(5, 54, 12), // "paddingRight"
QT_MOC_LITERAL(6, 67, 13), // "setPaddingTop"
QT_MOC_LITERAL(7, 81, 10), // "paddingTop"
QT_MOC_LITERAL(8, 92, 16), // "setPaddingBottom"
QT_MOC_LITERAL(9, 109, 13), // "paddingBottom"
QT_MOC_LITERAL(10, 123, 10), // "setPadding"
QT_MOC_LITERAL(11, 134, 7), // "padding"
QT_MOC_LITERAL(12, 142, 18), // "setIconLeftPadding"
QT_MOC_LITERAL(13, 161, 19), // "setIconRightPadding"
QT_MOC_LITERAL(14, 181, 17), // "setIconTopPadding"
QT_MOC_LITERAL(15, 199, 20), // "setIconBottomPadding"
QT_MOC_LITERAL(16, 220, 14), // "setIconPadding"
QT_MOC_LITERAL(17, 235, 12), // "setTextAlign"
QT_MOC_LITERAL(18, 248, 9), // "TextAlign"
QT_MOC_LITERAL(19, 258, 9), // "textAlign"
QT_MOC_LITERAL(20, 268, 15), // "setShowTriangle"
QT_MOC_LITERAL(21, 284, 12), // "showTriangle"
QT_MOC_LITERAL(22, 297, 14), // "setTriangleLen"
QT_MOC_LITERAL(23, 312, 11), // "triangleLen"
QT_MOC_LITERAL(24, 324, 19), // "setTrianglePosition"
QT_MOC_LITERAL(25, 344, 16), // "TrianglePosition"
QT_MOC_LITERAL(26, 361, 16), // "trianglePosition"
QT_MOC_LITERAL(27, 378, 16), // "setTriangleColor"
QT_MOC_LITERAL(28, 395, 13), // "triangleColor"
QT_MOC_LITERAL(29, 409, 11), // "setShowIcon"
QT_MOC_LITERAL(30, 421, 8), // "showIcon"
QT_MOC_LITERAL(31, 430, 12), // "setIconSpace"
QT_MOC_LITERAL(32, 443, 9), // "iconSpace"
QT_MOC_LITERAL(33, 453, 11), // "setIconSize"
QT_MOC_LITERAL(34, 465, 8), // "iconSize"
QT_MOC_LITERAL(35, 474, 13), // "setIconNormal"
QT_MOC_LITERAL(36, 488, 10), // "iconNormal"
QT_MOC_LITERAL(37, 499, 12), // "setIconHover"
QT_MOC_LITERAL(38, 512, 9), // "iconHover"
QT_MOC_LITERAL(39, 522, 12), // "setIconCheck"
QT_MOC_LITERAL(40, 535, 9), // "iconCheck"
QT_MOC_LITERAL(41, 545, 15), // "setIconPosition"
QT_MOC_LITERAL(42, 561, 12), // "IconPosition"
QT_MOC_LITERAL(43, 574, 7), // "iconPos"
QT_MOC_LITERAL(44, 582, 11), // "setShowLine"
QT_MOC_LITERAL(45, 594, 8), // "showLine"
QT_MOC_LITERAL(46, 603, 12), // "setLineSpace"
QT_MOC_LITERAL(47, 616, 9), // "lineSpace"
QT_MOC_LITERAL(48, 626, 12), // "setLineWidth"
QT_MOC_LITERAL(49, 639, 9), // "lineWidth"
QT_MOC_LITERAL(50, 649, 15), // "setLinePosition"
QT_MOC_LITERAL(51, 665, 12), // "LinePosition"
QT_MOC_LITERAL(52, 678, 12), // "linePosition"
QT_MOC_LITERAL(53, 691, 12), // "setLineColor"
QT_MOC_LITERAL(54, 704, 9), // "lineColor"
QT_MOC_LITERAL(55, 714, 16), // "setNormalBgColor"
QT_MOC_LITERAL(56, 731, 13), // "normalBgColor"
QT_MOC_LITERAL(57, 745, 15), // "setHoverBgColor"
QT_MOC_LITERAL(58, 761, 12), // "hoverBgColor"
QT_MOC_LITERAL(59, 774, 15), // "setCheckBgColor"
QT_MOC_LITERAL(60, 790, 12), // "checkBgColor"
QT_MOC_LITERAL(61, 803, 18), // "setNormalTextColor"
QT_MOC_LITERAL(62, 822, 15), // "normalTextColor"
QT_MOC_LITERAL(63, 838, 17), // "setHoverTextColor"
QT_MOC_LITERAL(64, 856, 14), // "hoverTextColor"
QT_MOC_LITERAL(65, 871, 17), // "setCheckTextColor"
QT_MOC_LITERAL(66, 889, 14), // "checkTextColor"
QT_MOC_LITERAL(67, 904, 16), // "setNormalBgBrush"
QT_MOC_LITERAL(68, 921, 13), // "normalBgBrush"
QT_MOC_LITERAL(69, 935, 15), // "setHoverBgBrush"
QT_MOC_LITERAL(70, 951, 12), // "hoverBgBrush"
QT_MOC_LITERAL(71, 964, 15), // "setCheckBgBrush"
QT_MOC_LITERAL(72, 980, 12), // "checkBgBrush"
QT_MOC_LITERAL(73, 993, 15), // "iconPaddingLeft"
QT_MOC_LITERAL(74, 1009, 16), // "iconPaddingRight"
QT_MOC_LITERAL(75, 1026, 14), // "iconPaddingTop"
QT_MOC_LITERAL(76, 1041, 17), // "iconPaddingBottom"
QT_MOC_LITERAL(77, 1059, 12), // "iconPosition"
QT_MOC_LITERAL(78, 1072, 14), // "TextAlign_Left"
QT_MOC_LITERAL(79, 1087, 15), // "TextAlign_Right"
QT_MOC_LITERAL(80, 1103, 13), // "TextAlign_Top"
QT_MOC_LITERAL(81, 1117, 16), // "TextAlign_Bottom"
QT_MOC_LITERAL(82, 1134, 16), // "TextAlign_Center"
QT_MOC_LITERAL(83, 1151, 21), // "TrianglePosition_Left"
QT_MOC_LITERAL(84, 1173, 22), // "TrianglePosition_Right"
QT_MOC_LITERAL(85, 1196, 20), // "TrianglePosition_Top"
QT_MOC_LITERAL(86, 1217, 23), // "TrianglePosition_Bottom"
QT_MOC_LITERAL(87, 1241, 17), // "IconPosition_Left"
QT_MOC_LITERAL(88, 1259, 18), // "IconPosition_Right"
QT_MOC_LITERAL(89, 1278, 16), // "IconPosition_Top"
QT_MOC_LITERAL(90, 1295, 19), // "IconPosition_Bottom"
QT_MOC_LITERAL(91, 1315, 17), // "LinePosition_Left"
QT_MOC_LITERAL(92, 1333, 18), // "LinePosition_Right"
QT_MOC_LITERAL(93, 1352, 16), // "LinePosition_Top"
QT_MOC_LITERAL(94, 1369, 19) // "LinePosition_Bottom"

    },
    "NavButton\0setPaddingLeft\0\0paddingLeft\0"
    "setPaddingRight\0paddingRight\0setPaddingTop\0"
    "paddingTop\0setPaddingBottom\0paddingBottom\0"
    "setPadding\0padding\0setIconLeftPadding\0"
    "setIconRightPadding\0setIconTopPadding\0"
    "setIconBottomPadding\0setIconPadding\0"
    "setTextAlign\0TextAlign\0textAlign\0"
    "setShowTriangle\0showTriangle\0"
    "setTriangleLen\0triangleLen\0"
    "setTrianglePosition\0TrianglePosition\0"
    "trianglePosition\0setTriangleColor\0"
    "triangleColor\0setShowIcon\0showIcon\0"
    "setIconSpace\0iconSpace\0setIconSize\0"
    "iconSize\0setIconNormal\0iconNormal\0"
    "setIconHover\0iconHover\0setIconCheck\0"
    "iconCheck\0setIconPosition\0IconPosition\0"
    "iconPos\0setShowLine\0showLine\0setLineSpace\0"
    "lineSpace\0setLineWidth\0lineWidth\0"
    "setLinePosition\0LinePosition\0linePosition\0"
    "setLineColor\0lineColor\0setNormalBgColor\0"
    "normalBgColor\0setHoverBgColor\0"
    "hoverBgColor\0setCheckBgColor\0checkBgColor\0"
    "setNormalTextColor\0normalTextColor\0"
    "setHoverTextColor\0hoverTextColor\0"
    "setCheckTextColor\0checkTextColor\0"
    "setNormalBgBrush\0normalBgBrush\0"
    "setHoverBgBrush\0hoverBgBrush\0"
    "setCheckBgBrush\0checkBgBrush\0"
    "iconPaddingLeft\0iconPaddingRight\0"
    "iconPaddingTop\0iconPaddingBottom\0"
    "iconPosition\0TextAlign_Left\0TextAlign_Right\0"
    "TextAlign_Top\0TextAlign_Bottom\0"
    "TextAlign_Center\0TrianglePosition_Left\0"
    "TrianglePosition_Right\0TrianglePosition_Top\0"
    "TrianglePosition_Bottom\0IconPosition_Left\0"
    "IconPosition_Right\0IconPosition_Top\0"
    "IconPosition_Bottom\0LinePosition_Left\0"
    "LinePosition_Right\0LinePosition_Top\0"
    "LinePosition_Bottom"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_NavButton[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      38,   14, // methods
      31,  330, // properties
       4,  423, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  204,    2, 0x0a /* Public */,
       4,    1,  207,    2, 0x0a /* Public */,
       6,    1,  210,    2, 0x0a /* Public */,
       8,    1,  213,    2, 0x0a /* Public */,
      10,    1,  216,    2, 0x0a /* Public */,
      10,    4,  219,    2, 0x0a /* Public */,
      12,    1,  228,    2, 0x0a /* Public */,
      13,    1,  231,    2, 0x0a /* Public */,
      14,    1,  234,    2, 0x0a /* Public */,
      15,    1,  237,    2, 0x0a /* Public */,
      16,    1,  240,    2, 0x0a /* Public */,
      16,    4,  243,    2, 0x0a /* Public */,
      17,    1,  252,    2, 0x0a /* Public */,
      20,    1,  255,    2, 0x0a /* Public */,
      22,    1,  258,    2, 0x0a /* Public */,
      24,    1,  261,    2, 0x0a /* Public */,
      27,    1,  264,    2, 0x0a /* Public */,
      29,    1,  267,    2, 0x0a /* Public */,
      31,    1,  270,    2, 0x0a /* Public */,
      33,    1,  273,    2, 0x0a /* Public */,
      35,    1,  276,    2, 0x0a /* Public */,
      37,    1,  279,    2, 0x0a /* Public */,
      39,    1,  282,    2, 0x0a /* Public */,
      41,    1,  285,    2, 0x0a /* Public */,
      44,    1,  288,    2, 0x0a /* Public */,
      46,    1,  291,    2, 0x0a /* Public */,
      48,    1,  294,    2, 0x0a /* Public */,
      50,    1,  297,    2, 0x0a /* Public */,
      53,    1,  300,    2, 0x0a /* Public */,
      55,    1,  303,    2, 0x0a /* Public */,
      57,    1,  306,    2, 0x0a /* Public */,
      59,    1,  309,    2, 0x0a /* Public */,
      61,    1,  312,    2, 0x0a /* Public */,
      63,    1,  315,    2, 0x0a /* Public */,
      65,    1,  318,    2, 0x0a /* Public */,
      67,    1,  321,    2, 0x0a /* Public */,
      69,    1,  324,    2, 0x0a /* Public */,
      71,    1,  327,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,    3,    5,    7,    9,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,    3,    5,    7,    9,
    QMetaType::Void, 0x80000000 | 18,   19,
    QMetaType::Void, QMetaType::Bool,   21,
    QMetaType::Void, QMetaType::Int,   23,
    QMetaType::Void, 0x80000000 | 25,   26,
    QMetaType::Void, QMetaType::QColor,   28,
    QMetaType::Void, QMetaType::Bool,   30,
    QMetaType::Void, QMetaType::Int,   32,
    QMetaType::Void, QMetaType::QSize,   34,
    QMetaType::Void, QMetaType::QPixmap,   36,
    QMetaType::Void, QMetaType::QPixmap,   38,
    QMetaType::Void, QMetaType::QPixmap,   40,
    QMetaType::Void, 0x80000000 | 42,   43,
    QMetaType::Void, QMetaType::Bool,   45,
    QMetaType::Void, QMetaType::Int,   47,
    QMetaType::Void, QMetaType::Int,   49,
    QMetaType::Void, 0x80000000 | 51,   52,
    QMetaType::Void, QMetaType::QColor,   54,
    QMetaType::Void, QMetaType::QColor,   56,
    QMetaType::Void, QMetaType::QColor,   58,
    QMetaType::Void, QMetaType::QColor,   60,
    QMetaType::Void, QMetaType::QColor,   62,
    QMetaType::Void, QMetaType::QColor,   64,
    QMetaType::Void, QMetaType::QColor,   66,
    QMetaType::Void, QMetaType::QBrush,   68,
    QMetaType::Void, QMetaType::QBrush,   70,
    QMetaType::Void, QMetaType::QBrush,   72,

 // properties: name, type, flags
       3, QMetaType::Int, 0x00095103,
       5, QMetaType::Int, 0x00095103,
       7, QMetaType::Int, 0x00095103,
       9, QMetaType::Int, 0x00095103,
      19, 0x80000000 | 18, 0x0009510b,
      73, QMetaType::Int, 0x00095003,
      74, QMetaType::Int, 0x00095003,
      75, QMetaType::Int, 0x00095003,
      76, QMetaType::Int, 0x00095003,
      77, 0x80000000 | 42, 0x0009510b,
      21, QMetaType::Bool, 0x00095103,
      23, QMetaType::Int, 0x00095103,
      26, 0x80000000 | 25, 0x0009510b,
      28, QMetaType::QColor, 0x00095103,
      30, QMetaType::Bool, 0x00095103,
      32, QMetaType::Int, 0x00095103,
      34, QMetaType::QSize, 0x00095103,
      36, QMetaType::QPixmap, 0x00095103,
      38, QMetaType::QPixmap, 0x00095103,
      40, QMetaType::QPixmap, 0x00095103,
      45, QMetaType::Bool, 0x00095103,
      47, QMetaType::Int, 0x00095103,
      49, QMetaType::Int, 0x00095103,
      52, 0x80000000 | 51, 0x0009510b,
      54, QMetaType::QColor, 0x00095103,
      56, QMetaType::QColor, 0x00095103,
      58, QMetaType::QColor, 0x00095103,
      60, QMetaType::QColor, 0x00095103,
      62, QMetaType::QColor, 0x00095103,
      64, QMetaType::QColor, 0x00095103,
      66, QMetaType::QColor, 0x00095103,

 // enums: name, flags, count, data
      18, 0x0,    5,  439,
      25, 0x0,    4,  449,
      42, 0x0,    4,  457,
      51, 0x0,    4,  465,

 // enum data: key, value
      78, uint(NavButton::TextAlign_Left),
      79, uint(NavButton::TextAlign_Right),
      80, uint(NavButton::TextAlign_Top),
      81, uint(NavButton::TextAlign_Bottom),
      82, uint(NavButton::TextAlign_Center),
      83, uint(NavButton::TrianglePosition_Left),
      84, uint(NavButton::TrianglePosition_Right),
      85, uint(NavButton::TrianglePosition_Top),
      86, uint(NavButton::TrianglePosition_Bottom),
      87, uint(NavButton::IconPosition_Left),
      88, uint(NavButton::IconPosition_Right),
      89, uint(NavButton::IconPosition_Top),
      90, uint(NavButton::IconPosition_Bottom),
      91, uint(NavButton::LinePosition_Left),
      92, uint(NavButton::LinePosition_Right),
      93, uint(NavButton::LinePosition_Top),
      94, uint(NavButton::LinePosition_Bottom),

       0        // eod
};

void NavButton::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        NavButton *_t = static_cast<NavButton *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setPaddingLeft((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->setPaddingRight((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->setPaddingTop((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->setPaddingBottom((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->setPadding((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->setPadding((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 6: _t->setIconLeftPadding((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->setIconRightPadding((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->setIconTopPadding((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->setIconBottomPadding((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->setIconPadding((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->setIconPadding((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 12: _t->setTextAlign((*reinterpret_cast< const TextAlign(*)>(_a[1]))); break;
        case 13: _t->setShowTriangle((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->setTriangleLen((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->setTrianglePosition((*reinterpret_cast< const TrianglePosition(*)>(_a[1]))); break;
        case 16: _t->setTriangleColor((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 17: _t->setShowIcon((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->setIconSpace((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->setIconSize((*reinterpret_cast< const QSize(*)>(_a[1]))); break;
        case 20: _t->setIconNormal((*reinterpret_cast< const QPixmap(*)>(_a[1]))); break;
        case 21: _t->setIconHover((*reinterpret_cast< const QPixmap(*)>(_a[1]))); break;
        case 22: _t->setIconCheck((*reinterpret_cast< const QPixmap(*)>(_a[1]))); break;
        case 23: _t->setIconPosition((*reinterpret_cast< const IconPosition(*)>(_a[1]))); break;
        case 24: _t->setShowLine((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->setLineSpace((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 26: _t->setLineWidth((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: _t->setLinePosition((*reinterpret_cast< const LinePosition(*)>(_a[1]))); break;
        case 28: _t->setLineColor((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 29: _t->setNormalBgColor((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 30: _t->setHoverBgColor((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 31: _t->setCheckBgColor((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 32: _t->setNormalTextColor((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 33: _t->setHoverTextColor((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 34: _t->setCheckTextColor((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 35: _t->setNormalBgBrush((*reinterpret_cast< const QBrush(*)>(_a[1]))); break;
        case 36: _t->setHoverBgBrush((*reinterpret_cast< const QBrush(*)>(_a[1]))); break;
        case 37: _t->setCheckBgBrush((*reinterpret_cast< const QBrush(*)>(_a[1]))); break;
        default: ;
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        NavButton *_t = static_cast<NavButton *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->getPaddingLeft(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->getPaddingRight(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->getPaddingTop(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->getPaddingBottom(); break;
        case 4: *reinterpret_cast< TextAlign*>(_v) = _t->getTextAlign(); break;
        case 5: *reinterpret_cast< int*>(_v) = _t->getIconPaddingLeft(); break;
        case 6: *reinterpret_cast< int*>(_v) = _t->getIconPaddingRight(); break;
        case 7: *reinterpret_cast< int*>(_v) = _t->getIconPaddingTop(); break;
        case 8: *reinterpret_cast< int*>(_v) = _t->getIconPaddingBottom(); break;
        case 9: *reinterpret_cast< IconPosition*>(_v) = _t->getIconPos(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->getShowTriangle(); break;
        case 11: *reinterpret_cast< int*>(_v) = _t->getTriangleLen(); break;
        case 12: *reinterpret_cast< TrianglePosition*>(_v) = _t->getTrianglePosition(); break;
        case 13: *reinterpret_cast< QColor*>(_v) = _t->getTriangleColor(); break;
        case 14: *reinterpret_cast< bool*>(_v) = _t->getShowIcon(); break;
        case 15: *reinterpret_cast< int*>(_v) = _t->getIconSpace(); break;
        case 16: *reinterpret_cast< QSize*>(_v) = _t->getIconSize(); break;
        case 17: *reinterpret_cast< QPixmap*>(_v) = _t->getIconNormal(); break;
        case 18: *reinterpret_cast< QPixmap*>(_v) = _t->getIconHover(); break;
        case 19: *reinterpret_cast< QPixmap*>(_v) = _t->getIconCheck(); break;
        case 20: *reinterpret_cast< bool*>(_v) = _t->getShowLine(); break;
        case 21: *reinterpret_cast< int*>(_v) = _t->getLineSpace(); break;
        case 22: *reinterpret_cast< int*>(_v) = _t->getLineWidth(); break;
        case 23: *reinterpret_cast< LinePosition*>(_v) = _t->getLinePosition(); break;
        case 24: *reinterpret_cast< QColor*>(_v) = _t->getLineColor(); break;
        case 25: *reinterpret_cast< QColor*>(_v) = _t->getNormalBgColor(); break;
        case 26: *reinterpret_cast< QColor*>(_v) = _t->getHoverBgColor(); break;
        case 27: *reinterpret_cast< QColor*>(_v) = _t->getCheckBgColor(); break;
        case 28: *reinterpret_cast< QColor*>(_v) = _t->getNormalTextColor(); break;
        case 29: *reinterpret_cast< QColor*>(_v) = _t->getHoverTextColor(); break;
        case 30: *reinterpret_cast< QColor*>(_v) = _t->getCheckTextColor(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        NavButton *_t = static_cast<NavButton *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setPaddingLeft(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setPaddingRight(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setPaddingTop(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setPaddingBottom(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setTextAlign(*reinterpret_cast< TextAlign*>(_v)); break;
        case 5: _t->setIconLeftPadding(*reinterpret_cast< int*>(_v)); break;
        case 6: _t->setIconRightPadding(*reinterpret_cast< int*>(_v)); break;
        case 7: _t->setIconTopPadding(*reinterpret_cast< int*>(_v)); break;
        case 8: _t->setIconBottomPadding(*reinterpret_cast< int*>(_v)); break;
        case 9: _t->setIconPosition(*reinterpret_cast< IconPosition*>(_v)); break;
        case 10: _t->setShowTriangle(*reinterpret_cast< bool*>(_v)); break;
        case 11: _t->setTriangleLen(*reinterpret_cast< int*>(_v)); break;
        case 12: _t->setTrianglePosition(*reinterpret_cast< TrianglePosition*>(_v)); break;
        case 13: _t->setTriangleColor(*reinterpret_cast< QColor*>(_v)); break;
        case 14: _t->setShowIcon(*reinterpret_cast< bool*>(_v)); break;
        case 15: _t->setIconSpace(*reinterpret_cast< int*>(_v)); break;
        case 16: _t->setIconSize(*reinterpret_cast< QSize*>(_v)); break;
        case 17: _t->setIconNormal(*reinterpret_cast< QPixmap*>(_v)); break;
        case 18: _t->setIconHover(*reinterpret_cast< QPixmap*>(_v)); break;
        case 19: _t->setIconCheck(*reinterpret_cast< QPixmap*>(_v)); break;
        case 20: _t->setShowLine(*reinterpret_cast< bool*>(_v)); break;
        case 21: _t->setLineSpace(*reinterpret_cast< int*>(_v)); break;
        case 22: _t->setLineWidth(*reinterpret_cast< int*>(_v)); break;
        case 23: _t->setLinePosition(*reinterpret_cast< LinePosition*>(_v)); break;
        case 24: _t->setLineColor(*reinterpret_cast< QColor*>(_v)); break;
        case 25: _t->setNormalBgColor(*reinterpret_cast< QColor*>(_v)); break;
        case 26: _t->setHoverBgColor(*reinterpret_cast< QColor*>(_v)); break;
        case 27: _t->setCheckBgColor(*reinterpret_cast< QColor*>(_v)); break;
        case 28: _t->setNormalTextColor(*reinterpret_cast< QColor*>(_v)); break;
        case 29: _t->setHoverTextColor(*reinterpret_cast< QColor*>(_v)); break;
        case 30: _t->setCheckTextColor(*reinterpret_cast< QColor*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject NavButton::staticMetaObject = {
    { &QPushButton::staticMetaObject, qt_meta_stringdata_NavButton.data,
      qt_meta_data_NavButton,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *NavButton::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *NavButton::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_NavButton.stringdata0))
        return static_cast<void*>(this);
    return QPushButton::qt_metacast(_clname);
}

int NavButton::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QPushButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 38)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 38;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 38)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 38;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 31;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
