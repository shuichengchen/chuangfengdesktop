/********************************************************************************
** Form generated from reading UI file 'ChuangfengDesktop.ui'
**
** Created by: Qt User Interface Compiler version 5.9.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHUANGFENGDESKTOP_H
#define UI_CHUANGFENGDESKTOP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ChuangfengDesktopClass
{
public:

    void setupUi(QWidget *ChuangfengDesktopClass)
    {
        if (ChuangfengDesktopClass->objectName().isEmpty())
            ChuangfengDesktopClass->setObjectName(QStringLiteral("ChuangfengDesktopClass"));
        ChuangfengDesktopClass->resize(600, 400);

        retranslateUi(ChuangfengDesktopClass);

        QMetaObject::connectSlotsByName(ChuangfengDesktopClass);
    } // setupUi

    void retranslateUi(QWidget *ChuangfengDesktopClass)
    {
        ChuangfengDesktopClass->setWindowTitle(QApplication::translate("ChuangfengDesktopClass", "ChuangfengDesktop", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ChuangfengDesktopClass: public Ui_ChuangfengDesktopClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHUANGFENGDESKTOP_H
